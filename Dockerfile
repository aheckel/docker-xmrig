FROM debian:buster-slim AS builder

ARG DEBIAN_FRONTEND=noninteractive
ARG XMRIG_VERSION

RUN apt update \
 && apt install -y git build-essential cmake libhwloc-dev libmicrohttpd-dev libssl-dev libuv1-dev \
 && git clone --depth 1 --branch "v${XMRIG_VERSION}" https://github.com/xmrig/xmrig.git /tmp/xmrig \
 && cd /tmp/xmrig \
 && mkdir build \
 && cd build \
 && cmake -DCMAKE_BUILD_TYPE=Release .. \
 && make

FROM debian:buster-slim

LABEL maintainer="Alexander Heckel <alexander.heckel@icloud.com>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update \
 && apt install -y --no-install-recommends libhwloc-dev libmicrohttpd-dev libssl-dev libuv1-dev \
 && rm -rf /var/lib/apt/lists/*

COPY --from=builder /tmp/xmrig/build/xmrig /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/xmrig"]
CMD ["--help"]
