# Multi Arch Build

```bash
export XMRIG_VERSION=6.9.0
docker buildx build --build-arg XMRIG_VERSION --platform linux/arm64,linux/amd64 -t aheckel/xmrig:"${XMRIG_VERSION}" --push .
```

# Run
```bash
docker run -it --rm aheckel/xmrig:"${XMRIG_VERSION}"
```

## Examples
```bash
docker run -d --rm \
  aheckel/xmrig:"${XMRIG_VERSION}" \
  -o <pool>:<port> -u <wallet_address>+<woker_name> [--tls]
```
